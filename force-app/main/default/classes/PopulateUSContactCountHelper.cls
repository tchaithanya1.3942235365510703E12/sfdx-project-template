public with Sharing class PopulateUSContactCountHelper {

    public static void updateAccountsOnContactInsert(List<Contact> conts){
        List<Account> listAccount = new List<Account>();
        Map<Id, Integer> contactCount = new Map<Id, Integer>();        
        for(Contact cont : conts){            
            if(cont.MailingCountry == 'USA'){
                if(contactCount.containsKey(cont.AccountId)){
                    contactCount.put(cont.AccountId, contactCount.get(cont.AccountId)+1);
                }
                else
                {
                    contactCount.put(cont.AccountId, 1);
                }                    
            }
        }
        List<Account> accounts = [Select id, Number_of_US_Contacts__c from Account where Id IN:contactCount.keySet()];
        for(Account a : accounts){
            if(a.Number_of_US_Contacts__c != Null)
                a.Number_of_US_Contacts__c  += contactCount.get(a.id);
            else
                a.Number_of_US_Contacts__c = contactCount.get(a.id);
        }
        try{
            update accounts;
        }catch(Exception e){
            System.debug('Exception happened at update'+e);
        }       
                
    }
    
    public static void updateAccountsOnContactDelete(List<Contact> conts){
        List<Account> listAccount = new List<Account>();
        Map<Id, Integer> contactCount = new Map<Id, Integer>();        
        for(Contact cont : conts){            
            if(cont.MailingCountry == 'USA'){
                if(contactCount.containsKey(cont.AccountId)){
                    contactCount.put(cont.AccountId, contactCount.get(cont.AccountId)+1);
                }
                else
                {
                    contactCount.put(cont.AccountId, 1);
                }                    
            }
        }
        List<Account> accounts = [Select id, Number_of_US_Contacts__c from Account where Id IN:contactCount.keySet()];
        for(Account a : accounts){
            if(a.Number_of_US_Contacts__c != Null)
                a.Number_of_US_Contacts__c  -= contactCount.get(a.id);
        }
        try{
            update accounts;
        }catch(Exception e){
            System.debug('Exception happened at update'+e);
        }       
                
    }
    
    public static void updateAccountsOnContactUpdate(List<Contact> conts, Map<id, Contact> oldContactsMap){
        List<Account> listAccount = new List<Account>();
        Map<Id, Integer> contactCount = new Map<Id, Integer>();        
        for(Contact cont : conts){            
            if(cont.MailingCountry == 'USA' && oldContactsMap.get(cont.id).MailingCountry != 'USA'){
                if(contactCount.containsKey(cont.AccountId)){
                    contactCount.put(cont.AccountId, contactCount.get(cont.AccountId)+1);
                }
                else
                {
                    contactCount.put(cont.AccountId, 1);
                }                    
            }
            else if(cont.MailingCountry != 'USA' && oldContactsMap.get(cont.id).MailingCountry == 'USA'){
                if(contactCount.containsKey(cont.AccountId)){
                    contactCount.put(cont.AccountId, contactCount.get(cont.AccountId)-1);
                }
                else
                {
                    contactCount.put(cont.AccountId, -1);
                }                 
            }
        }
        List<Account> accounts = [Select id, Number_of_US_Contacts__c from Account where Id IN:contactCount.keySet()];
        for(Account a : accounts){
            if(a.Number_of_US_Contacts__c != Null)
                a.Number_of_US_Contacts__c  += contactCount.get(a.id);
        }
        try{
            update accounts;
        }catch(Exception e){
            System.debug('Exception happened at update'+e);
        }       
                
    }

}