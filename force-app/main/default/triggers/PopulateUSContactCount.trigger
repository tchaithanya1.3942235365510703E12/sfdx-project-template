trigger PopulateUSContactCount on Contact (after insert, after update, after delete) {

    if(Trigger.isInsert){
        PopulateUSContactCountHelper.updateAccountsOnContactInsert(Trigger.new);
    }
    
    if(Trigger.isUpdate){
		PopulateUSContactCountHelper.updateAccountsOnContactUpdate(Trigger.new, Trigger.oldMap);        
    }
    if(Trigger.isDelete){
        PopulateUSContactCountHelper.updateAccountsOnContactDelete(Trigger.old);
    }
}